SRCS_DIR = srcs
OBJS_DIR = objs
INCS_DIR = includes

SRCS = qxml.c\
	   xml_load.c

OBJS = $(addprefix $(OBJS_DIR)/, $(SRCS:.c=.o))

NAME = libqxml.a
RM = rm -rf
CC = gcc

CFLAGS = -Werror -Wall -Wextra -g

.PHONY: clean test

all: $(OBJS_DIR) $(NAME)

$(NAME): $(OBJS)
	@ar rsc $(NAME) $(OBJS)

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.c $(INCS_DIR)/qxml.h
	@printf "CC \033[1m$<\033[0m\n"
	@$(CC) -fPIC -c -I $(INCS_DIR) $(CFLAGS) -o $@ $<

$(OBJS_DIR):
	@printf "=== $(NAME)\n"
	mkdir $(OBJS_DIR)

test: all
	@$(CC) -I $(INCS_DIR) test/main.c -L. -lqxml -o $@/$@
	@./$@/$@

clean:
	$(RM) $(OBJS_DIR)

fclean: clean
	@$(RM) $(NAME)

debug: CFLAGS = -DDEBUG
debug: all

re: fclean all
