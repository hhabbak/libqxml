#ifndef QXML_H
# define QXML_H

typedef struct s_qxml	t_qxml;

struct			s_qxml
{
	char	*name;
	char	*value;
	t_qxml	*child;
	t_qxml	*next;
};

void	qxml_print(t_qxml *node);
void	qxml_free(t_qxml *node);
void	qxml_loadstr(t_qxml **node, char *str);
int		qxml_loadfile(t_qxml **node, char *path);
void	qxml_add(t_qxml **node, char *name, char *value);
void	qxml_init(t_qxml *node);
t_qxml	qxml_findnode(t_qxml *node, char *key);

#endif
