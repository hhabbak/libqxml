#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "qxml.h"

void test_dir(char *path)
{
	DIR *dir;
	struct dirent *entry;
	char path_file[4096];
	t_qxml *qxml;

	if (!(dir = opendir(path)))
		return;
	while ((entry = readdir(dir)) != NULL)
	{
		if (entry->d_type != DT_DIR)
		{
			strcpy(path_file, path);
			strcat(path_file, entry->d_name);
			printf("[!] %s\n", path_file);
			qxml_loadfile(&qxml, path_file);
			qxml_print(qxml);
			qxml_free(qxml);
			qxml = NULL;
		}
	}
	closedir(dir);
}

int main(int argc, char **argv)
{
	t_qxml *node;
	char *end;
	char xml_dir[4096];

	end = strrchr(argv[0], '/');
	strncpy(xml_dir, argv[0], end - argv[0]);
	strcat(xml_dir, "/");
	strcat(xml_dir, "xml/");
	test_dir(xml_dir);
	return 0;
}
