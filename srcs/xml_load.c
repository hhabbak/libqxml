#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "qxml.h"

static char	*strtrim(char const *s)
{
	char	*res;
	size_t	len;

	if (!s)
		return (NULL);
	while (*s == ' ' || *s == '\n' || *s == '\t')
		s++;
	len = strlen(s);
	while ((s[len - 1] == ' ' || s[len - 1] == '\n' || s[len - 1] == '\t')
			&& len)
		len--;
	res = malloc(len + 1);
	if (!res)
		return (NULL);
	res[len] = '\0';
	while (len--)
		res[len] = s[len];
	return (res);
}

static char	*get_tag_end(char *tag_name)
{
	char *res;

	res = malloc(sizeof(char) * strlen(tag_name) + 4);
	if (!res)
		return (NULL);
	strcpy(res, "</");
	strcat(res, tag_name);
	strcat(res, ">");
	return (res);
}

static char	*get_content(char *beg, char *end, char *content_end,
			char *tag_end)
{
	char *content;
	char *tmp;

	tmp = strndup(end + 1, content_end - beg - strlen(tag_end) + 1);
	if (!tmp)
		return (NULL);
	content = strtrim(tmp);
	free(tmp);
	return (content);
}

static void	tag_free(char *tag_name, char *tag_end)
{
	free(tag_name);
	free(tag_end);
}

static void	node_get(t_qxml **node, char *beg, char *end, char *tag_name)
{
	char	*tag_end;
	char	*content_end;
	char	*content;

	if (!(tag_end = get_tag_end(tag_name)))
	{
		free(tag_name);
		return ;
	}
	if (!(content_end = strstr(end, tag_end))
		|| !(content = get_content(beg, end, content_end, tag_end)))
	{
		tag_free(tag_name, tag_end);
		return ;
	}
	qxml_loadstr(node, content_end + strlen(tag_end));
	qxml_add(node, tag_name, NULL);
	qxml_loadstr(&(*node)->child, content);
	if (!(*node)->child)
		(*node)->value = strdup(content);
	tag_free(tag_name, tag_end);
	free(content);
}

void		qxml_loadstr(t_qxml **node, char *str)
{
	char	*beg;
	char	*end;
	char	*tag_name;

	tag_name = NULL;
	beg = strchr(str, '<');
	if (!beg)
		return ;
	end = strchr(beg, '>');
	if (!end)
		return ;
	tag_name = strndup(beg + 1, end - beg - 1);
	if (!tag_name)
		return ;
	node_get(node, beg, end, tag_name);
}

int qxml_loadfile(t_qxml **node, char *path)
{
	char *str;
	size_t size;
	int fd;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return -1;
	size = lseek(fd, 0, SEEK_END);
	str = malloc(sizeof(char) * size + 1);
	if (!str)
	{
		close(fd);
		return -1;
	}
	lseek(fd, 0, SEEK_SET);
	read(fd, str, size);
	qxml_loadstr(node, str);
	free(str);
	close(fd);
	return 0;
}
