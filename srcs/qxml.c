#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "qxml.h"

void	qxml_init(t_qxml *node)
{
	node->name = NULL;
	node->value = NULL;
	node->child = NULL;
	node->next = NULL;
}

void	qxml_add(t_qxml **node, char *name, char *value)
{
	t_qxml *new;

	if (node)
	{
		new = malloc(sizeof(*new));
		if (!new)
			return ;
		qxml_init(new);
		new->name = strdup(name);
		if (value)
			new->value = strdup(value);
		new->next = *node;
		*node = new;
	}
}

t_qxml	qxml_findnode(t_qxml *node, char *key)
{
	t_qxml tmp;

	if (!node)
	{
		qxml_init(&tmp);
		return (tmp);
	}
	if (strcmp(node->name, key) == 0)
		return (*node);
	return (qxml_findnode(node->next, key));
}

void	qxml_free(t_qxml *node)
{
	if (!node)
		return ;
	if (node->child)
		qxml_free(node->child);
	qxml_free(node->next);
	free(node->name);
	free(node->value);
	free(node);
}

static void	print_current(t_qxml *node, size_t level)
{
	size_t i;

	i = 0;
	while (i++ < level)
		printf("    ");
	printf("%s", node->name);
	if (node->value)
	{
		printf(" : ");
		printf("%s", node->value);
	}
	printf("\n");
}

static void	print_node(t_qxml *node, size_t level)
{
	if (!node)
		return ;
	print_current(node, level);
	if (node->child)
		print_node(node->child, level + 1);
	print_node(node->next, level);
}

void		qxml_print(t_qxml *node)
{
	print_node(node, 0);
}
